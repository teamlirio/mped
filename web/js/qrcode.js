function onClearForm() {

	$("label").remove(".error");
	$(this).find("input").val('').end();

}
$(document).ready(function() {
	$("#form").on('hidden.bs.modal', function() {
		$("label").remove(".error");
		$(this).find("input").val('').end();
	});
});

function submitform() {
	jQuery.validator.addMethod("notNumber", function(value, element, param) {
		var reg = /[0-9]/;
		if (reg.test(value)) {
			return false;
		} else {
			return true;
		}
	}, "Number is not allowed.");
	$.validator.addMethod(
		    "regex",
		    function(value, element, regexp) {
		        var check = false;
		        var re = new RegExp(regexp);
		        return this.optional(element) || re.test(value);
		    },
		    "No special Characters allowed here. Use only upper and lowercase letters (A through Z; a through z), numbers and punctuation marks (. , : ; ? ' ' \" - = ~ ! @ # $ % ^ & * ( ) _ + / < > { } )"
		);

	$("#registrationform").validate({
		rules : {
			mobNumber : {
				regex :  /^[0-9A-Za-z\d]+$/,
				minlength : 10,
				maxlength : 11,
				number : true,
				required : true
			},
			lname : {
				maxlength: 20,
				regex :  /^[-, A-Za-z\d]+$/,
				required : true,
				notNumber : true
			},
			fname : {
				maxlength: 20,
				regex : /^[-, A-Za-z\d]+$/,
				required : true,
				notNumber : true
			},
			brand : {
				maxlength: 40,
				regex : /^[-, A-Za-z\d]+$/,
				required : true,
				notNumber : true
			},
			position : {
				maxlength: 40,
				regex :  /^[-, A-Za-z\d]+$/,
				required : true,
				notNumber : true
			},
			email : {
				required : true,
				email : true
			},

		},
		messages : {
			mobNumber : {
				regex : "Special Characters are not allowed",
				number : "Only numbers are allowed.",
				required : "Mobile number is required"
			},
			lname : {
				maxlength : "Maximum of 20 characters only",
				regex : "Special Characters are not allowed",
				required : "Last name is required",
				notNumber : "Number is not allowed"
			},
			fname : {
				maxlength : "Maximum of 20 characters only",
				regex : "Special Characters are not allowed",
				required : "First name is required",
				notNumber : "Number is not allowed"
			},
			email : {
				
				required : "Email is required",
				email : "Please enter a valid email"
			},
			brand : {
				maxlength : "Maximum of 40 characters only",
				regex : "Special Characters are not allowed",
				required : "Brand is required",
				notNumber : "Number is not allowed"
			},
			position : {
				maxlength : "Maximum of 40 characters only",
				regex : "Special Characters are not allowed",
				required : "Position is required",
				notNumber : "Number is not allowed"
			},
		},
		submitHandler : function(form) {
			// alert('dasdasdasd');
		}
	});

	// Submit the FORM
	if ($("#registrationform").valid()) {
		// alert("Hello world");

		$("#registrationform").submit(function(e) {
			console.log($(this).serializeArray());
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type : "POST",
				data : postData,
				success : function(data, textStatus, jqXHR) {
					// if success
					$('#successful2').modal('hide');
					$('#successful').modal('show');
					
					var json = $.parseJSON(data);
					$("#img-status").attr("src", json.img);
					$("#successTitle").html(json.title);
					$("#successBlock").html(json.block);
					
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$('#successful2').modal('hide');
					$('#successful').modal('show');
					$("#successTitle").html("Ooops!");
					$("#successBlock").html("Something went wrong.");
				
				}
			});
			e.preventDefault(); // STOP default action
			e.stopImmediatePropagation();
			// e.unbind(); //unbind. to stop multiple form submit.
		});
		$("#registrationform").submit();
		$('#successful2').modal('show');
		
		$('#form').modal('hide');
		$("#registrationform")[0].reset();
		$("#registrationform").reset();
		
		

	}

}
