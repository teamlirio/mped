package com.fluxion.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.hibernate.Session;

import com.fluxion.dao.RewardCodeDAO;
import com.fluxion.util.HibernateUtil;

public class RewardCodeDAOImpl implements RewardCodeDAO {
	
	public String getRewardCode(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String assignCode =  "";

		try {
			session.beginTransaction();
			List res = session.createQuery("Select codes from Codes where visitorid = :vid")
					.setParameter("vid", id)
					.list();
			session.getTransaction().commit();
			for(int ctr = 0; ctr < res.size(); ctr++) {
				System.out.println("Size " + res.size() + "Values" + res.get(ctr) );
			}
			if(res.size() == 1) {
				
				assignCode = res.get(0).toString();
				System.out.println(assignCode);
			}
			return assignCode;
		} catch (Exception e) {
			System.out.println("Get Code Exception");
			e.printStackTrace();
		} finally {
			session.close();

		}

		return assignCode;
	}
}
