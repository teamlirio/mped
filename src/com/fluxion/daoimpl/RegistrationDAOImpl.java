package com.fluxion.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.fluxion.beans.Visitor;
import com.fluxion.dao.RegistrationDAO;
import com.fluxion.util.HibernateUtil;

public class RegistrationDAOImpl implements RegistrationDAO {
	
	

	public RegistrationDAOImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean checkExist(String mobNumber, String email) {
		boolean isExist = false;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
		
			session.beginTransaction();
			int count = 1;
			
			List query = session.createQuery("FROM Visitor where mobilenumber = ? OR email = ?")
					.setParameter(0, mobNumber)
					.setParameter(1, email)
					.list();
	
			session.getTransaction().commit();
			if (query.size() > 0) {
				isExist = true;
				System.out.println("User is already in the database.");

			} else {
				isExist = false;
				System.out.println("User does not exist.");

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Something went wrong.");
		} finally { 
			session.close();
			
		}
		return isExist;
	}

	@Override
	public int saveToDb(HashMap<String, String> hash) {
		// TODO Auto-generated method stub
		Session session = HibernateUtil.getSessionFactory().openSession();
		int isSaved = 0;
		String fName = hash.get("Firstname");
		String lName = hash.get("Lastname");
		String brand = hash.get("Brand");
		String position = hash.get("Position");
		String mobNumber = hash.get("MobileNumber");
		String email = hash.get("Email");
		if(!checkExist(mobNumber, email)) {
			try {
				System.out.println("Saving to DB..");
				Visitor visitor = new Visitor(fName, lName, brand, position, email, mobNumber);
				session.beginTransaction();
				session.save(visitor);
				session.getTransaction().commit();
				isSaved = retrieveID(mobNumber, email);
				if(assignCode(isSaved)) {
					return isSaved;
				}				
		
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Something went wrong during saving of details to database.");
			}  finally { 
				session.close();
			
			}
		}
		return isSaved;
	}
	
	public int retrieveID(String mNumber, String email) {
		int id = 0;
		System.out.println("Pulling ID from last insert.");
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			List list = session.createQuery("Select distinct id from Visitor where mobilenumber = :mNum AND email = :eml")
			.setParameter("mNum", mNumber)
			.setParameter("eml", email)
			.list();
			session.getTransaction().commit();
			if(list.size() == 1) {
				id = Integer.parseInt(list.get(0).toString());
				System.out.println("ID: " + id);
			} else {
				System.out.println("Failed to get ID. ID is = " + id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Something went wrong during getting of ID.");
		} finally { 
			session.close();
			
		}
		return id;
	}
	
	public boolean rollBack(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		boolean rollbackSuccess = false;
		try {
			session.beginTransaction();
			int delVisitor = session.createQuery("DELETE from Visitor where id = :id")
					.setParameter("id", id)
					.executeUpdate()
					;
			int updCodes = session.createQuery("UPDATE Codes set visitorid = NULL where visitorid = :id")
					.setParameter("id", id)
					.executeUpdate();
			session.getTransaction().commit();
			
			if(delVisitor == 1 && updCodes == 1) {
				rollbackSuccess = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Roll back failed.");
		} finally {
			session.close();
			
		}
		return rollbackSuccess;
	}
	@Override
	public boolean assignCode(int visitorId) {
		boolean updateSuccess = false;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			System.out.println("Assigning code to visitorId " + visitorId);
			session.beginTransaction();
			
			Query getCodeId = session.createQuery("SELECT id from Codes where visitorid is null").setMaxResults(1);
			String codeId = getCodeId.getResultList().get(0).toString();
			System.out.println("getCodeId: " + codeId);
			
			Query query = session.createQuery("UPDATE Codes set visitorid = :vid where id = :codeId")
//			Query query = session.createQuery("UPDATE Codes set visitorid = :vid where visitorid is null")
					.setParameter("vid", visitorId)
					.setParameter("codeId", Integer.parseInt(codeId));
			
			int res = query.executeUpdate();
			session.getTransaction().commit();
			System.out.println("RES: " + res);
			if(res != 0) {
				updateSuccess = true;
			}
			//return updateSuccess;
		} catch(Exception e) {
			System.out.println("Assign code exception.");
			e.printStackTrace();
		} finally {
			session.close();
			
		}
		return updateSuccess;
	}

	public static void main(String[] args) {
//		HashMap<String, String> hash = new HashMap<String, String>();
//		hash.put("Designation", "MR");
//		hash.put("Firstname", "normz");
//		hash.put("Lastname", "lirio");
//		hash.put("Brand", "Fluxion");
//		hash.put("Position", "Dev");
//		hash.put("MobileNumber", "090648105123342");
//		hash.put("Email", "nor12312mz@flu123xiom.com.ph");
//		new RegistrationDAOImpl().saveToDb(hash);
	

	}

}
