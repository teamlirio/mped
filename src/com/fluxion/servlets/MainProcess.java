package com.fluxion.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fluxion.util.Helper;
import com.fluxion.dao.RegistrationDAO;
import com.fluxion.dao.Test;
import com.fluxion.daoimpl.RegistrationDAOImpl;
import com.fluxion.util.HibernateUtil;

/**
 * Servlet implementation class MainProcess
 */
@WebServlet("/mainprocess")
public class MainProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainProcess() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html"); //$NON-NLS-1$
		PrintWriter out = response.getWriter();
		StringWriter sr = new StringWriter();
		// String qrCodeData = request.getParameter("qrText");
		String designation = request.getParameter("designation"); //$NON-NLS-1$
		String fname = request.getParameter("fname"); //$NON-NLS-1$
		String lname = request.getParameter("lname"); //$NON-NLS-1$
		String brand = request.getParameter("brand"); //$NON-NLS-1$
		String position = request.getParameter("position"); //$NON-NLS-1$
		String mobNumber = request.getParameter("mobNumber"); //$NON-NLS-1$
		String email = request.getParameter("email"); //$NON-NLS-1$

		System.out.println("##########################################"); //$NON-NLS-1$
		System.out.println("Registration recieved"); //$NON-NLS-1$
		System.out.println("First Name	: " + fname); //$NON-NLS-1$
		System.out.println("Last Name	: " + lname); //$NON-NLS-1$
		System.out.println("Brand		: " + brand); //$NON-NLS-1$
		System.out.println("Position	: " + position); //$NON-NLS-1$
		System.out.println("Mobile No.	: " + mobNumber); //$NON-NLS-1$
		System.out.println("Email Add	: " + email); //$NON-NLS-1$
		System.out.println("##########################################"); //$NON-NLS-1$

		HashMap<String, String> hash = new HashMap<String, String>();
		hash.put("Designation", designation); //$NON-NLS-1$
		hash.put("Firstname", fname); //$NON-NLS-1$
		hash.put("Lastname", lname); //$NON-NLS-1$
		hash.put("Brand", brand); //$NON-NLS-1$
		hash.put("Position", position); //$NON-NLS-1$
		hash.put("MobileNumber", mobNumber); //$NON-NLS-1$
		hash.put("Email", email); //$NON-NLS-1$

		System.out.println(new Test().hello());
		RegistrationDAO register = new RegistrationDAOImpl();
		int id = register.saveToDb(hash);

		if (id > 0) {
			System.out.println("Successfully added to database"); //$NON-NLS-1$
			HashMap<String, String> hashMap = sendReward(email, mobNumber, id);
			if (hashMap != null) {
				System.out.println("Successful API call.");
				String responseBody = hashMap.get("ResponseBody");
				System.out.println("Response Body :" + responseBody);
				if (responseBody.equalsIgnoreCase("200")) {
					out.println();
					out.println("{");
					out.println("\"img\": \"images/check.png\",");
					out.println("\"title\": \"Thank you!\",");
					out.println("\"block\": \"" + Messages.getString("RES.200")
							+ "\"");
					out.println("}");
					out.flush();
					out.close();

				} else if (responseBody.equalsIgnoreCase("415")) {
					// Response body is not 200
					if (register.rollBack(id)) {
						System.out.println("API CALL - Invalid Mobile number");
						System.out.println("Roll back success!");
					}
					

					out.println();
					out.println("{");
					out.println("\"img\": \"images/wrong.png\",");
					out.println("\"title\": \"Ooops!\",");
					out.println("\"block\": \"" + Messages.getString("RES.415")
							+ "\"");
					out.println("}");
					out.close();
				} else if (responseBody.equalsIgnoreCase("401")) {
					// Internal Error
					if (register.rollBack(id)) {
						System.out.println("API CALL - Internal Error has occured");
						System.out.println("Roll back success!");
					}
					

					out.println();
					out.println("{");
					out.println("\"img\": \"images/wrong.png\",");
					out.println("\"title\": \"Ooops!\",");
					out.println("\"block\": \"" + Messages.getString("RES.401")
							+ "\"");
					out.println("}");
					out.close();

				} else if (responseBody.equalsIgnoreCase("400")) {
					// Invalid Credentials
					if (register.rollBack(id)) {
						System.out.println("API CALL - Invalid Credentials");
						System.out.println("Roll back success!");
					}				
					out.println();
					out.println("{");
					out.println("\"img\": \"images/wrong.png\",");
					out.println("\"title\": \"Ooops!\",");
					out.println("\"block\": \"" + Messages.getString("RES.400")
							+ "\"");
					out.println("}");
					out.close();
				} else if (responseBody.equalsIgnoreCase("410")) {
					// Invalid Sender
					if (register.rollBack(id)) {
						System.out.println("API CALL - Invalid Sender.");
						System.out.println("Roll back success!");
					}
					
					out.println();
					out.println("{");
					out.println("\"img\": \"images/wrong.png\",");
					out.println("\"title\": \"Ooops!\",");
					out.println("\"block\": \"" + Messages.getString("RES.410")
							+ "\"");
					out.println("}");
					out.close();
				}

			} else {
				System.out
						.println("HashMap is Null. Failed to connect to API.");
			}
			out.close();

		} else {
			// User is existing
			System.out.println("User is already in the database.");
			out.println();
			out.println("{");
			out.println("\"img\": \"images/wrong.png\",");
			out.println("\"title\": \"OooPS!\",");
			out.println("\"block\": \"" + Messages.getString("RES.Exist")
					+ "\"");
			out.println("}");
			out.close();
		}

	}

	private HashMap<String, String> sendReward(String emailAdd,
			String mobNumber, int id) throws IOException {
		// :TODO Connect to API for rewards..
		System.out.println("Connecting to API..."); //$NON-NLS-1$
		HashMap<String, String> hash = new HashMap<>();
		URL obj = new URL(Messages.getString("API.URL")); //$NON-NLS-1$
		String params = new Helper().generateRequestURL(mobNumber, id);

		String user_agent = "Mozilla/5.0"; //$NON-NLS-1$
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST"); //$NON-NLS-1$
			con.setRequestProperty("User-Agent", user_agent); //$NON-NLS-1$

			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(params.getBytes());
			os.flush();
			os.close();

			int responseCode = con.getResponseCode();
			BufferedReader _buff = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String _InputLine;
			while ((_InputLine = _buff.readLine()) != null) {
				response.append(_InputLine);
			}

			hash.put("ResponseCode", String.valueOf(responseCode)); //$NON-NLS-1$
			hash.put("ResponseBody", response.toString()); //$NON-NLS-1$
			System.out.println("Response Code " + responseCode); //$NON-NLS-1$
			return hash;
		} catch (Exception e) {

		} finally {
			con.disconnect();
		}

		return hash;
	}

}
