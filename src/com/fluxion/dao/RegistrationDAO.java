package com.fluxion.dao;

import java.util.HashMap;

public interface RegistrationDAO {

	public abstract int saveToDb(HashMap<String, String> hash);

	public abstract boolean checkExist(String mobNumber, String email);

	public abstract boolean assignCode(int id);
	
	public abstract int retrieveID(String mNum, String email);
	
	public abstract boolean rollBack(int id);

}