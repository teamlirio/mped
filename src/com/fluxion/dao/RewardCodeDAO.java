package com.fluxion.dao;

public interface RewardCodeDAO {
	public abstract String getRewardCode(int id);
}